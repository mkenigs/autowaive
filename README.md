# autowaive

Automatic waiving of unstable tests.

## Maintainers

* [jracek](https://gitlab.com/jracek)
* 1 rack empty, feel free to sign youselves up!

## Description

This tool is supposed to be run in a CI (e.g. each week/month).
It will waive a test in `kpet-db` if the test is having too many false positives within
a given timeframe.

This tool pulls data from DataWarehouse, which is currently an internal instance.
With a little bit of work, it can be made to use a different datasource.

## Setup

`GITLAB_URL` (likely `https://gitlab.com/`) and `GITLAB_PRIVATE_TOKEN`
environment variables have to be set. The token allows you to create MRs
for `cki-project/kpet-db`.

File `conf.yaml` has to contain the endpoint for datasource (DataWarehouse)
and a comment template for autowaiving. See `conf.yaml.template`, which
is pretty self-explanatory.

## Common usage

Please consider `./cli.py --help` to be your friend. There are common
arguments being used, in order to make the tool and the documentation not overly
inflated.

### Listing DataWarehouse tests info

* Get DataWarehouse test id of LTP lite test
 
`$ ./cli.py listtests | grep -i "LTP lite"`
    
    41 LTP lite        
    3 LTP lite

The reasons why there are multiple test ids for LTP. By now, DW should
contain unique entries only.

* Get test id from Data Warehouse 

    $ ./cli.py listtests | grep -i "connectathon"`
    42 CIFS Connectathon
    76 NFS Connectathon
    852 NFS Connectathon
    830 CIFS Connectathon

### Getting test false positives rate

* Get (print) false positive rates of all tests within last month.

`$ ./cli.py analyze`

The report of false positive rate looks like this (for each test):

    Results for Test (19) stress: stress-ng
    -X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X
    Infra 5                                                                         
    Unidentified 5                                                                  
    Kernel Bug 9                                                                    
    False positives rate is: 35.71 %
 
### Autowaiving a test

#### Waive a specific test

Get LTP lite test (test id == 3) failures from datawarehouse since 2020/02/01.
If false positives threshold is above 15%, waive the test using `waive.py` 
by cloning kpet-db (default behavior) and opening a merge request with
automatic commit message.

`$ ./cli.py -s 2020/02/01 waive -i 3 --thres 15`

Obviously such a threshold is way too low and this is only to explain how
to use this command.

You can also waive tests by their testname.

`$ ./cli.py -s 2020/02/01 waive -t "LTP lite" --thres 15`

#### Waive any test 

Waive any test within last month that has false positives rate higher than 15%.
Create a separate merge request for each test.

`$ ./cli.py waive --thres 15`

## Computation of false positive rate for a test

DataWarehouse uses "tags" for any failure entered into it.
This tool sorts failures for a test by such tags.
Here's an example report again:

    Results for Test (19) stress: stress-ng
    -X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X
    Infra 5                                                                         
    Unidentified 5                                                                  
    Kernel Bug 9                                                                    
    False positives rate is: 35.71 %

The result is computed as: 

` count('Unstable') + count('Unidentified') / count('total_test_runs')  * 100` [%]

