#!/usr/bin/env python3
"""Auto-waive tests based on DW data/stats."""
# pylint: disable=C0103,R0913,R0914
import datetime
import sys
import re

import click
from dateutil.parser import parse as date_parse
from cki_lib.misc import init_logger
from cki_lib.misc import safe_popen

from autowaive import processing
from autowaive import const

LOGGER = init_logger(__name__, dst_file='autowaive.log')


@click.group()
@click.option('-s', '--since', default=None,
              help='analyze tests within timeframe (since -> onward)')
@click.option('--days', default=14, type=int,
              help='Process data within timeframe of last X days.')
@click.option('--full', default=False, type=bool, is_flag=True,
              help='Print report even about tests that we do not '
                   'have enough data to decide false positives rate.')
@click.pass_context
def cli(ctx, since, days, full):
    """Click cli entry point."""
    # ensure that ctx.obj exists and is a dict (in case `cli()` is called
    # by means other than the `if` block below
    ctx.ensure_object(dict)

    since = date_parse(since) if since else None

    # if not using since, override to get last month's data
    if days and since is None:
        since = datetime.datetime.now() - datetime.timedelta(days=days)
        since = date_parse(str(since))

    if since:
        print(f'Getting test failures since {since}')

    ctx.obj['since'] = since
    ctx.obj['full'] = full


@cli.command()
@click.option('-t', '--testname', required=False, multiple=True,
              help='Instead of analyzing all tests, analyze a single or'
                   ' multiple tests by name')
@click.option('-i', '--testid', required=False, multiple=True, type=int,
              help='Instead of analyzing all tests, analyze a single or'
                   ' multiple tests by their id')
@click.pass_context
def analyze(ctx, testname, testid):
    """Analyze false positive rate of tests."""
    test_lists = processing.get_test_lists(testid=testid, testname=testname)

    # grab data from datawarehouse
    results = list(processing.go_pool(test_lists, ctx.obj['since'], None))

    fp_rate_results = processing.compute_fp_rate(results)
    for test, rate, stats in fp_rate_results:
        msg = None
        if rate is not None:
            msg = f'False positives rate is: {rate:.2f} %\n'
        elif ctx.obj['full']:
            msg = f'Not enough results to calculate false-positives rate\n'

        if msg is not None:
            print(f'Results for Test ({test.id}) {test.name}')
            print('-X' * 40)
            # print count of issues
            print(f'Total runs {stats.total_runs}')
            for key, issue in stats.issues.items():
                print(key, issue.reports)
            print(msg)


def do_waive(db, test, days, extra_args, rate, thres):
    """Do actual waiving of a test."""
    # let user know what's going on
    print(f'* AUTOWAIVE {test.name} ({int(rate)}% >= {thres}%)')

    # convert maintainer emails to Gitlab mentions
    maintainer_nicks = [x['email'].split('@')[0] for x in
                        test.maintainers]
    maintainers = ' '.join([f'@{x}' for x in maintainer_nicks])

    comment = const.WAIVE_COMMENT.format(maintainers=maintainers,
                                         rate=int(rate), thres=thres,
                                         days=days)

    # append -c comment to MR description, mentioning test maintainers
    extra_args += ['-c', comment]
    # construct a shell command
    args = ['./waive.py', '--db', db] + extra_args + \
           [f'{test.name}']
    LOGGER.debug(args)

    # run it with no redirections, stdout and stderr will be
    # printed directly
    _, _, returncode = safe_popen(args)
    if returncode:
        # let user know what's happening
        print(f'something went wrong while waiving {test.name}')

        return False

    return True


@cli.command()
@click.option('-t', '--testname', required=False, multiple=True,
              help='Instead of analyzing all tests, analyze a single or'
                   ' multiple tests by name')
@click.option('-i', '--testid', required=False, multiple=True, type=int,
              help='Instead of analyzing all tests, analyze a single or'
                   ' multiple tests by their id')
@click.option('--thres', required=True, type=int, default=80,
              help='Percentage threshold. Auto-waive test if ')
@click.option('--db', required=False, type=str, default='./',
              help='Path to kpet-db.')
@click.option('--remote/--no-remote', required=False, default=True,
              help='Waiving: if used, then clone the kpet-db repo to temp'
                   ' directory.')
@click.option('--mr/--no-mr', required=False, default=True,
              help='Waiving: if used, then create a merge request.')
@click.option('--auto/--no-auto', required=False, default=True,
              help='Waiving: if used, then use default commit message.')
@click.option('-e', '--exclude', required=False, default=[], multiple=True,
              help='Regex, can be specified multiple times. Excludes tests'
                   ' that matches this regex(es).')
@click.pass_context
def waive(ctx, testname, testid, thres, db, remote, mr, auto, exclude):
    """Analyze false positive rate of tests."""
    extra_args = []
    extra_args += ['-r'] if remote else extra_args
    extra_args += ['-m'] if mr else extra_args
    extra_args += ['-a'] if auto else extra_args
    regexes = [re.compile(regex) for regex in exclude]

    days = ctx.parent.params['days']

    test_lists = processing.get_test_lists(testid=testid, testname=testname)

    # grab data from datawarehouse
    results = list(processing.go_pool(test_lists, ctx.obj['since'], None))
    fp_rate_results = processing.compute_fp_rate(results)

    our_retcode = 0
    for test, rate, _ in fp_rate_results:
        dont_waive = False
        for regex in regexes:
            if regex.match(test.name):
                dont_waive = True
                break

        if dont_waive:
            print(f'* EXCLUDE "{test.name}" using command-line')
            continue

        if rate is not None:
            if int(rate) >= thres:
                if not do_waive(db, test, days, extra_args, rate,
                                thres):
                    # override exit code
                    our_retcode = 1
            else:
                print(f'* IGNORE {test.name} ({int(rate)}% < {thres}%)')

    # distinguish 2 retcodes (0 - all good, 1 - something went wrong)
    sys.exit(our_retcode)


@cli.command()
@click.pass_context
def listtests(ctx):
    """List tests in DataWarehouse."""
    since = ctx.obj['since']

    test_list = processing.get_tests(limit=None, since=since)

    for test in test_list:
        print(test.id, test.name)


if __name__ == '__main__':
    # pylint: disable=E1120
    cli()
