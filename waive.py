#!/usr/bin/env python3
"""Waive or unwaive a test in kpet-db."""
import argparse
import io
import os
import re
import subprocess
import sys
import tempfile

from cki_lib import misc
from cki_lib.gitlab import GitlabHelper

from cki_lib.yaml_wrap import RuamelSerializer

LOGGER = misc.init_logger(__name__, dst_file='waive.log')


def modify_waived_flag(unwaive, case):
    """Set or remove waived flag for case."""
    try:
        is_waived = case['waived']
    except KeyError:
        is_waived = False

    if unwaive and is_waived:
        del case['waived']
    elif not is_waived:
        # flip the flag to true
        case['waived'] = True


def find_and_waive_casename(yaml_wrap, fname, case_name, unwaive):
    """Try to find casename in fname. If found, set the waived flag."""
    # parse yaml file
    waived_anything = False
    parsed_yaml = yaml_wrap.deserialize_file(fname)
    try:
        cases = parsed_yaml['cases']
    except KeyError:
        pass
    else:
        for case in cases:
            try:
                current_case_name = parsed_yaml['name']
            except KeyError:
                sys.stderr.write(f'failed to waive "{case_name}" !\n')
                continue

            if current_case_name == case_name:
                # log that we found the case name
                LOGGER.warning(f'case name "{case_name} found in {fname}')

                # set or remove waived flag for case
                modify_waived_flag(unwaive, case)

                # write the file
                with io.open(fname, 'w', encoding='utf8') as out_stream:
                    yaml_wrap.serialize(parsed_yaml, out_stream)

    return waived_anything


def waive_unwaive(args):
    """Do any actual waiving/unwaiving of a case."""
    # instantiate the (de)serializer we're going to use
    yaml_wrap = RuamelSerializer()
    waived_anything = False
    for root, _, files in os.walk(args.db):
        for file in files:
            if file.endswith(".yaml"):
                fname = os.path.join(root, file)

                # log what file we're processing
                LOGGER.info(f'processing file "{fname}"')

                # find the casename, waive it and write the file
                if find_and_waive_casename(yaml_wrap, fname, args.casename,
                                           args.unwaive):
                    waived_anything = True

    return waived_anything


def create_mr(args, dst_dir, gitlab_helper):
    """Force push a branch and create a merge request."""
    target_branch = ('un-' if args.unwaive else '') + f'waive-{args.casename}'
    target_branch += f'_{next(tempfile._get_candidate_names())}'
    # normalize branch name
    target_branch = re.sub(r'[^a-zA-Z0-9-_]', '_', target_branch)

    # log what's happening
    LOGGER.info(f'MR will be pushed to origin/{target_branch}')

    with misc.enter_dir(dst_dir):
        # make a commit
        commit_msg_args = []
        if args.a:
            commit_msg_args = ['-m', f"autowaive {args.casename}"]
        misc.safe_popen(['git', 'commit', '-sa'] + commit_msg_args)

        stdout, stderr, retcode = misc.safe_popen(
            ['git', 'push', '-f', 'origin',
             f'master:{target_branch}'],
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE)
    if retcode:
        LOGGER.warning(stdout)
        LOGGER.error(stderr)

        raise RuntimeError('failed to create a merge request')

    merge_request = gitlab_helper.project.mergerequests.create(
        {'source_branch': target_branch,
         'target_branch': 'master',
         'title': f'autowaive {args.casename}',
         'labels': []})

    merge_request.description = f"autowaive {args.casename}"
    if args.c:
        merge_request.description += f'\n\n{args.c}'
    merge_request.save()

    print(merge_request.web_url)


def main():
    """Main entry point, duh."""
    misc.truncate_logs()

    parser = argparse.ArgumentParser(
        description='Set waive tag to True for a case-name in kpet-db.'
    )
    parser.add_argument(
        '--db',
        help='Location of database of kernel trees and tests, default ".".'
             'Not necessary if --remote is used.',
        default='.',
    )
    parser.add_argument(
        '-r',
        '--remote',
        action='store_true',
        help='If used, then clone the repo to temp directory.',
        default=False,
    )
    parser.add_argument(
        '-m',
        action='store_true',
        help='If used, create a merge request.',
        default=False,
    )
    parser.add_argument(
        '-c',
        help='If used, add this comment to the merge request description.',
        default=None,
    )
    parser.add_argument(
        '-a',
        action='store_true',
        help='If used, then use automatic commit message.',
        default=False,
    )

    parser.add_argument(
        '-u',
        '--unwaive',
        action='store_true',
        help='If used, remove the waived flag instead.',
        default=False
    )
    parser.add_argument(
        'casename',
        help='The name of the case to waive in kpet-db.',
        default=None
    )
    args = parser.parse_args()

    if args.remote:
        url = os.environ['GITLAB_URL']
        token = os.environ['GITLAB_PRIVATE_TOKEN']
        project = 'cki-project/kpet-db'
        gitlab_helper = GitlabHelper(url, token)
        gitlab_helper.set_project(project)

        with tempfile.TemporaryDirectory() as dirpath:
            dst_dir = os.path.join(dirpath, 'kpet-db')

            # log what's happening
            LOGGER.info(f'cloning to {dst_dir}')

            # call git clone
            gitlab_helper.clone_project(dst_dir=dst_dir, protocol='http')

            # force kpet-db path to directory we created
            args.db = dst_dir

            # do actual waiving in kpet-db directory
            if waive_unwaive(args):
                # only create MR if we've waived anything
                if args.m:
                    create_mr(args, dst_dir, gitlab_helper)
            else:
                print(
                    f'* Not creating MR, nothing waived for {args.casename} !')
    else:
        # do actual waiving in kpet-db directory
        waive_unwaive(args)


if __name__ == '__main__':
    main()
