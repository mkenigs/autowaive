"""DataWarehouse datastructure validation for auto-waiving tests."""

from typing import List

from rcdefinition.rc_data import DefinitionBase, _NO_DEFAULT


# # # Listing Tests

class DWTests(DefinitionBase):
    """DW test."""
    id: int = _NO_DEFAULT
    name: str = _NO_DEFAULT
    fetch_url: str = _NO_DEFAULT
    maintainers: List[dict] = _NO_DEFAULT
    universal_id: str = _NO_DEFAULT


class DWTestListResults(DefinitionBase):
    """DW list of tests."""
    tests: List[DWTests] = _NO_DEFAULT


class DWTestList(DefinitionBase):
    """DW test listing."""
    count: int = _NO_DEFAULT
    next: str = _NO_DEFAULT
    previous: str = _NO_DEFAULT
    results: DWTestListResults = _NO_DEFAULT


# # # Getting test stats

class DWTestIssue(DefinitionBase):
    """DW test issue."""
    reports: int = _NO_DEFAULT
    rate: float = _NO_DEFAULT


class DWTestIssues(DefinitionBase):
    """DW test issues dict."""
    Infra: DWTestIssue = _NO_DEFAULT
    Kernel_Bug: DWTestIssue = _NO_DEFAULT
    Unstable_Test: DWTestIssue = _NO_DEFAULT
    Unidentified: DWTestIssue = _NO_DEFAULT
    Workflow_Problem: DWTestIssue = _NO_DEFAULT
    Pipeline_Bug: DWTestIssue = _NO_DEFAULT

    def __init__(self, dict_data):
        new_dict = {}
        for key in dict_data.keys():
            new_key = key.replace(' ', '_')
            new_dict[new_key] = dict_data[key]

        super(DWTestIssues, self).__init__(new_dict)


class DWIssueStats(DefinitionBase):
    """DW test stats."""
    test: DWTests = _NO_DEFAULT
    total_runs: int = _NO_DEFAULT
    total_failures: int = _NO_DEFAULT
    issues: DWTestIssues = _NO_DEFAULT
