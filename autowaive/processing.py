"""Process DW data/stats about test failures."""

import itertools
from urllib.parse import urljoin
import requests
import tqdm

from cki_lib.misc import init_logger
from cki_lib.retrying import retrying_on_exception
from cki_lib.pool import NonDaemonicPool

from autowaive import const
from autowaive.waivedata import DWTestList, DWIssueStats

LOGGER = init_logger(__name__, dst_file='info.log')


class ServerError(Exception):
    """Indicates HTTP 500 server error -> couple of retries -> skip test."""


@retrying_on_exception(ServerError, retries=2)
def get_next_data(url, crafter, since=None):
    """Fetch data since date from url, parse them using crafter class."""
    LOGGER.info(f'fetching {url}')

    params = {}
    if since is not None:
        params['since'] = since

    response = requests.get(url, params=params)
    if response.status_code == 500:
        raise ServerError('server error')

    try:
        parsed_data = crafter(response.json())
    except Exception:
        print(url)
        print(response)
        print(response.content)
        raise

    return parsed_data


def get_any_data(crafter, endpoint, since=None, limit=None):
    """Get paginated data from endpoint, parse using crafter class.

    Args:
        crafter: class like DWTestList that parses all data passed to it
        endpoint: str, url to fetch data from
        since: datetime, limits the timeframe of results
        limit: int, how many pages to parse, all if None
    """
    # construct url to get id & name for each test
    url = urljoin(const.DW_URLBASE, endpoint)

    # do http get
    try:
        data = get_next_data(url, crafter, since=since)
    except ServerError:
        data = None
    results = []

    if limit is None:
        # don't stop until we fetch all data or all data since timeframe
        break_condition = lambda x: False  # noqa: E731
    else:
        # break after we get #<limit> pages
        break_condition = lambda x: x > limit  # noqa: E731

    i = 0
    while True:
        if data:
            # save results
            results.append(data)
        else:
            break

        # use getattr, the object doesn't have to have .next attribute
        next_data = getattr(data, 'next', None)
        if next_data:
            # paging "next" not being nul means there's more results
            try:
                data = get_next_data(next_data, crafter, since=since)
            except ServerError:
                break

        if not next_data or break_condition(i):
            # stop processing on break condition
            break

        i += 1

    return results


def get_tests(since=None, limit=None):
    """Get all tests since timeframe."""
    result = get_any_data(DWTestList, const.LIST_TESTS_ENDPOINT, since=since,
                          limit=limit)

    return list(itertools.chain(*[test for test in
                                  [x.results.tests for x in result]]))


def get_test_fp_rate(tup):
    """Get test by test_id, return its failures since timeframe."""
    test, since, limit = tup
    # construct url: use a specific endpoint to get false-positives data
    endpoint = const.TEST_STATS_ENDPOINT.format(test_id=test.id)

    # fetch data from end-point, parse using DWIssueStats
    all_data = get_any_data(DWIssueStats, endpoint, since=since, limit=limit)
    if all_data:
        return (test, all_data[0])
    else:
        return (test, {})


def go_pool(test_list, since, limit):
    """Process data in non-daemonic process pool."""
    input_data = [(test, since, limit) for test in test_list]

    with NonDaemonicPool(100) as mypool:
        out_data = list(tqdm.tqdm(mypool.imap(get_test_fp_rate, input_data),
                                  total=len(input_data)))

        return filter(lambda x: x is not None, out_data)


def get_tag_value(source, key):
    """Get value from a dict, return its length or 0 if key doesn't exist."""
    try:
        value = len(source[key])
    except KeyError:
        value = 0
    return value


def compute_fp_rate(results):
    """Get list of tuples (test object, fp rate, partioned failure)."""
    fp_rate_results = []

    for test, stats in results:
        rate = None
        unstable = stats.issues.Unstable_Test.reports
        unidentified = stats.issues.Unidentified.reports
        total_runs = stats.total_runs
        if stats and (unstable + unidentified > 0):
            try:
                rate = float(((unstable + unidentified) / (total_runs)) * 100)
            except ZeroDivisionError:
                pass

        # (test, None, {}) is acceptable
        fp_rate_results.append((test, rate, stats))

    return fp_rate_results


def get_test_lists(testid=None, testname=None):
    """Get a list of test objects by testids, testnames."""
    # either user specified testids or testnames
    if testid or testname:
        test_lists = get_tests()
        if testname:
            # filter tests by name
            test_lists = list(filter(lambda x: x.name in testname, test_lists))
        elif testid:
            # filter tests by id
            test_lists = list(filter(lambda x: x.id in testid, test_lists))
    else:
        # otherwise get all tests
        test_lists = get_tests()

    tests = '\n'.join([x.name for x in test_lists]) + '\n'
    LOGGER.debug(f'Analyzing tests: {tests}')

    return test_lists
