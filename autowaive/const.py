"""Constants for auto-waiving tests."""
from cki_lib.yaml_wrap import RuamelSerializer

_WRAP = RuamelSerializer().deserialize_file(fname='conf.yaml')

WAIVE_COMMENT = _WRAP['WAIVE_COMMENT']
DW_URLBASE = _WRAP['DW_URLBASE']

LIST_TESTS_ENDPOINT = '/api/1/test'

TEST_STATS_ENDPOINT = '/api/1/test/{test_id}/stats'
